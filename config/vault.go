package config

import (
	"errors"
	"log"
	vapi "github.com/hashicorp/vault/api"
)

// FetchVaultSecrets retrieves secrets from vault using the provided key, token and address.
func FetchVaultSecrets(addr, token, key, loginURL, role string) (map[string]interface{}, error) {
	client, err := vapi.NewClient(&vapi.Config{
		Address: addr,
	})

	if err != nil {
		return nil, err
	}

	options := map[string]interface{}{
		"jwt": token,
		"role": role,
	}

	log.Printf("Connecting to Vault login endpoint: %s\n", loginURL)
	secretLogin, err := client.Logical().Write(loginURL, options)

	if err != nil {
		return nil, err
	}

	if len(secretLogin.Auth.ClientToken) == 0 {
		return nil, errors.New("client token was blank")
	}

	client.SetToken(secretLogin.Auth.ClientToken)
	secretRead, err := client.Logical().Read(key)

	if err != nil {
		return nil, err
	}

	return secretRead.Data, nil
}
