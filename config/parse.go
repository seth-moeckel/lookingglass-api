package config

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"bitbucket.org/crosschx/lookingglass-api/service/database"
	"bitbucket.org/crosschx/lookingglass-api/service/login"

	"github.com/kelseyhightower/envconfig"
)

// Config is the application config used for import from env
// and export to json (for the healthcheck)
type Config struct {
	DB    database.Config `envconfig:"DB" json:"database"`
	Vault struct {
		Host        string `json:"host"`
		Token       string `json:"-"`
		SecretsPath string `json:"secretsPath" split_words:"true"`
		LoginURL    string `json:"loginUrl"`
		Role        string `json:"role"`
	} `envconfig:"VAULT" json:"vault"`

	HTTP struct {
		ListenAddr string `split_words:"true" default:":5000"`
	} `envconfig:"HTTP" json:"http"`

	Service struct {
		APICredentials login.Credentials
	} `envconfig:"-" json:"-"`

	LogLevel     string `default:"info" json:"logLevel" split_words:"true"`
	Local        bool   `default:"false" json:"local"`
	MigrationDir string `default:"./migrations" split_words:"true" json:"-"`
	Env          string `json:"env"`
}

// Parse processes the environment variables required for configuration
// and checks that the log level provided is valid before returning a
// Config object.
func Parse() (Config, error) {
	config := Config{}
	err := envconfig.Process("lga", &config)
	if err != nil {
		return config, err
	}

	// manually handle default fields
	if config.Local {
		devCheckFields := map[string]string{
			"LGA_DB_USERNAME": config.DB.Username,
			"LGA_DB_PASSWORD": config.DB.Password,
		}

		for name, val := range devCheckFields {
			if val == "" {
				return Config{}, fmt.Errorf("required key %s missing value", name)
			}
		}
	}

	if !config.Local {
		if len(config.HTTP.ListenAddr) == 0 {
			config.HTTP.ListenAddr = "0.0.0.0"
		}

		jwtPath := os.Getenv("VAULT_JWT_PATH")
		if len(jwtPath) == 0 {
			jwtPath = "/var/run/secrets/kubernetes.io/serviceaccount/token"
		}
		data, err := ioutil.ReadFile(filepath.Clean((jwtPath)))
		if err != nil {
			return Config{}, fmt.Errorf("Could not read VAULT_JWT_PATH contents from file %s", jwtPath)
		}
		config.Vault.Token = string(data)

		if len(config.Vault.Token) == 0 {
			return Config{}, fmt.Errorf("Successfully read VAULT_JWT_PATH file %s but contents were empty", jwtPath)
		}

		config.Vault.LoginURL = os.Getenv("VAULT_LOGIN_URL")
		if len(config.Vault.LoginURL) == 0 {
			return Config{}, errors.New("VAULT_LOGIN_URL was not set")
		}

		config.Env = os.Getenv("POD_NAMESPACE")
		if len(config.Env) == 0 {
			return Config{}, errors.New("POD_NAMESPACE was not set")
		}
	}

	level := config.LogLevel
	if level != "info" && level != "warn" && level != "debug" && level != "error" {
		return Config{}, fmt.Errorf("unsupported logging level %s", level)
	}

	return config, nil
}
