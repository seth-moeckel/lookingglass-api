package config

import (
	"errors"

	"bitbucket.org/crosschx/lookingglass-api/service/login"
	"bitbucket.org/crosschx/lookingglass-api/service/vault"
)

// Fetch retrieves the application config for the current run.
func Fetch() (Config, error) {
	conf, err := Parse()
	if err != nil {
		return Config{}, err
	}

	if !conf.Local {
		secrets, err := FetchVaultSecrets(conf.Vault.Host, conf.Vault.Token, conf.Vault.SecretsPath, conf.Vault.LoginURL, conf.Vault.Role)
		if err != nil {
			return Config{}, err
		}

		username, ok := secrets["dbusername"].(string)
		if !ok {
			return Config{}, errors.New("secrets value returned missing field 'dbusername'")
		}

		password, ok := secrets["dbpassword"].(string)
		if !ok {
			return Config{}, errors.New("secrets value returned missing field 'dbpassword'")
		}

		conf.DB.Username = username
		conf.DB.Password = password

		v, err := vault.New(vault.Config{
			Host:     conf.Vault.Host,
			LoginKey: conf.Vault.LoginURL,
			Token:    conf.Vault.Token,
			Role:     conf.Vault.Role,
			Env:      conf.Env,
		})
		if err != nil {
			return Config{}, err
		}

		creds, err := login.FetchCreds(v)
		if err != nil {
			return Config{}, err
		}

		conf.Service.APICredentials = creds
	}

	return conf, nil
}
