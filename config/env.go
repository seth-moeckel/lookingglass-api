package config

// Env is used for string comparison when determining runtime environment.
type Env string

const (
	// LocalDev is the localdev environment
	LocalDev Env = "localdev"

	// Dev is the dev environment
	Dev Env = "dev"

	// QA is the qa environment
	QA Env = "qa"

	// Secure is the secure/prod environment
	Secure Env = "secure"

	// Integration is the integration environment
	Integration Env = "integration"
)

// BuildInfo is used to store values injected in the app entrypoint during build time
// and pass those values to the health check during runtime.
type BuildInfo struct {
	GitCommit string `json:"gitCommit"`
	GitBranch string `json:"gitBranch"`
	Version   string `json:"version"`
}
