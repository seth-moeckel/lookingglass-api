FROM golang:alpine as build
RUN apk --no-cache add ca-certificates

FROM scratch

COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY bin/api /api
COPY migrations migrations

EXPOSE 5000
ENTRYPOINT ["/api"]
