package middlewares

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/crosschx/lookingglass-api/service/log"

	"github.com/prometheus/client_golang/prometheus"
)

type statusWriter struct {
	status int
	http.ResponseWriter
}

func (w *statusWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

// RecordTrip times the request duration as well as the statusCode and
// reports those metrics to the prometheus handler.
func RecordTrip(logger log.Logger) func(http.Handler) http.Handler {
	requestsHistogram := prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "requests_histogram_secs",
			Help: "Request information",
		},
		[]string{"status_code", "path", "method"},
	)

	err := prometheus.Register(requestsHistogram)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to register prometheus requests collector")
	}

	return func(inner http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			writer := &statusWriter{ResponseWriter: res}

			startTime := time.Now()
			inner.ServeHTTP(writer, req)

			// record duration and check status code
			if writer.status == 0 {
				writer.status = 200
			}
			duration := time.Now().Sub(startTime)
			requestsHistogram.
				WithLabelValues(fmt.Sprintf("%d", writer.status), req.URL.Path, req.Method).
				Observe(duration.Seconds())

			if strings.HasPrefix(req.URL.Path, "/api/health/") {
				return
			}

			logger.Debug().
				Str("duration", duration.String()).
				Str("url", req.URL.String()).
				Int("status", writer.status).
				Str("method", req.Method).
				Msg("request served")
		})
	}
}
