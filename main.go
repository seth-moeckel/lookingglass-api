package main

import (
	"net/http"

	"bitbucket.org/crosschx/lookingglass-api/config"
	"bitbucket.org/crosschx/lookingglass-api/routes"
	"bitbucket.org/crosschx/lookingglass-api/service/database"
	"bitbucket.org/crosschx/lookingglass-api/service/log"
)

// injected by build (see Makefile)
var (
	GitCommit string
	GitBranch string
	Version   string
)

func main() {
	// Get configuration
	conf, err := config.Fetch()
	if err != nil {
		panic(err)
	}

	// Setup logger
	logger := log.New(conf.Local, "main", log.Level(conf.LogLevel))
	logger.Info().Str("git_commit", GitCommit).Str("git_branch", GitBranch).Str("version", Version).Msg("build information")

	buildInfo := config.BuildInfo{
		GitCommit: GitCommit,
		GitBranch: GitBranch,
		Version:   Version,
	}

	// Connect to database
	db, err := database.Dial(conf.DB)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to connect to database")
	}

	// Run database migrations
	err = db.RunMigrations(conf.MigrationDir, logger)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to execute database migrations")
	}

	logger.Info().Str("addr", conf.HTTP.ListenAddr).Msg("starting http listener")
	err = http.ListenAndServe(conf.HTTP.ListenAddr, routes.Generate(buildInfo, conf, logger, db))
	if err != nil {
		logger.Fatal().Err(err).Msg("http listener crashed")
	}
}
