package routes

import (
	"net/http"

	"bitbucket.org/crosschx/lookingglass-api/config"
	"bitbucket.org/crosschx/lookingglass-api/controller"
	"bitbucket.org/crosschx/lookingglass-api/middlewares"
	"bitbucket.org/crosschx/lookingglass-api/service/database"
	"bitbucket.org/crosschx/lookingglass-api/service/log"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	goji "goji.io"
	"goji.io/pat"
)

// Generate creates and returns the application router.
func Generate(buildInfo config.BuildInfo, conf config.Config, logger log.Logger, db *database.Database) http.Handler {
	mux := goji.NewMux()
	api := goji.SubMux()

	mux.Handle(pat.New("/api/*"), api)
	api.Use(middlewares.RecordTrip(logger))

	api.Handle(pat.New("/health/metrics"), promhttp.Handler())
	api.HandleFunc(pat.Get("/health/status"), controller.HealthCheck(buildInfo, conf, logger, db))

	api.HandleFunc(pat.Post("/jobs"), controller.CreateJob(logger, db))
	api.HandleFunc(pat.Get("/jobs"), controller.GetAllJobs(logger, db))
	api.HandleFunc(pat.Get("/jobs/next"), controller.GetNextJob(logger, db))
	api.HandleFunc(pat.Get("/jobs/:id"), controller.GetJobById(logger, db))
	api.HandleFunc(pat.Delete("/jobs/:id"), controller.DeleteJobById(logger, db))

	api.HandleFunc(pat.Put("/jobs/:id/status"), controller.SetJobStatus(logger, db))
	api.HandleFunc(pat.Put("/jobs/:id/keepalive"), controller.UpdateJobKeepAlive(logger, db))

	return mux
}
