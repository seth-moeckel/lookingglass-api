CREATE TABLE `job` (
  id VARCHAR(36),
  perspective_name VARCHAR(256) NOT NULL DEFAULT '',
  rulescript TEXT NOT NULL,
  status VARCHAR(10) NOT NULL DEFAULT 'queued',
  last_keep_alive timestamp NULL DEFAULT NULL,
  submitted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE `job_glob` (
  id VARCHAR(36),
  job_id VARCHAR(36) NOT NULL,
  glob TEXT NOT NULL,
  PRIMARY KEY (id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- ALTER TABLE `job` ADD CONSTRAINT UNIQUE(id);
-- ALTER TABLE `job` ADD CONSTRAINT UNIQUE(perspective_name);
-- ALTER TABLE `job` ADD INDEX (id, perspective_name);
-- ALTER TABLE `job_globs` ADD CONSTRAINT FOREIGN KEY (job_id) REFERENCES jobs(id) ON DELETE CASCADE;
