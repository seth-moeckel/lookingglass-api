package controller

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/crosschx/lookingglass-api/service/database"
	"bitbucket.org/crosschx/lookingglass-api/service/job"
	"bitbucket.org/crosschx/lookingglass-api/service/log"

	"goji.io/pat"
)

func CreateJob(logger log.Logger, db *database.Database) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		jobInput := job.JobInput{}
		err := json.NewDecoder(req.Body).Decode(&jobInput)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		job, err := job.Create(req.Context(), db, jobInput)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		err = json.NewEncoder(res).Encode(job)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})
}

func GetAllJobs(logger log.Logger, db *database.Database) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		jobs, err := job.GetAll(db)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		err = json.NewEncoder(res).Encode(jobs)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})
}

func GetNextJob(logger log.Logger, db *database.Database) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		job, err := job.GetNext(req.Context(), db)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		err = json.NewEncoder(res).Encode(job)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})
}

func GetJobById(logger log.Logger, db *database.Database) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		job, err := job.GetByID(req.Context(), db, pat.Param(req, "id"))
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		err = json.NewEncoder(res).Encode(job)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})
}

func DeleteJobById(logger log.Logger, db *database.Database) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		err := job.DeleteByID(req.Context(), db, pat.Param(req, "id"))
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})
}

func SetJobStatus(logger log.Logger, db *database.Database) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		status := job.JobStatus{}
		err := json.NewDecoder(req.Body).Decode(&status)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		err = job.UpdateStatusByID(req.Context(), db, pat.Param(req, "id"), status.Status)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})
}

func UpdateJobKeepAlive(logger log.Logger, db *database.Database) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		err := job.UpdateKeepAliveByID(req.Context(), db, pat.Param(req, "id"))
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})
}
