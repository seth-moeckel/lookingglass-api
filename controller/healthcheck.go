package controller

import (
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/crosschx/lookingglass-api/config"
	"bitbucket.org/crosschx/lookingglass-api/service/database"
	"bitbucket.org/crosschx/lookingglass-api/service/log"
)

// HealthResponse the reply from the healthcheck endpoint
type HealthResponse struct {
	StartTime time.Time        `json:"startTime"`
	Config    config.Config    `json:"config"`
	Build     config.BuildInfo `json:"build"`
}

// HealthCheck returns the application configuration and attempts to ping the database,
// returning a internal server error if unsuccessful.
func HealthCheck(buildInfo config.BuildInfo, conf config.Config, logger log.Logger, db *database.Database) http.HandlerFunc {
	healthResponse := HealthResponse{
		StartTime: time.Now(),
		Config:    conf,
		Build:     buildInfo,
	}

	json, err := json.Marshal(healthResponse)
	if err != nil {
		logger.Fatal().Err(err).Msg("call to json.Marshal on HealthResponse failed")
	}

	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		err := db.Ping()
		if err != nil {
			logger.Error().Err(err).Msg("Database connection broken, attempting to reconnect")

			// attempt to self-heal
			db, err = database.Dial(conf.DB)
			if err != nil {
				logger.Error().Err(err).Msg("failed to reconnect to database")
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}
		}

		res.Header().Set("Content-Type", "application/json")
		/* #nosec */
		res.Write(json)
	})
}
