package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"goji.io"
	"goji.io/pat"
)

func main() {

	mux.HandleFunc(pat.Post("/api/jobs"), func(res http.ResponseWriter, req *http.Request) {
		jobInput := JobInput{}
		err = json.NewDecoder(req.Body).Decode(&jobInput)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		job := Job{
			ID:              bson.NewObjectId().Hex(),
			PerspectiveName: jobInput.PerspectiveName,
			Rulescript:      jobInput.Rulescript,
			RuleGlobs:       jobInput.RuleGlobs,
			Status:          "queued",
			SubmittedAt:     time.Now().UTC(),
			LastKeepAlive:   time.Now().UTC(),
		}

		err = collection.Insert(job)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		err = json.NewEncoder(res).Encode(job)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})

	mux.HandleFunc(pat.Post("/api/jobs/:id/keepalive"), func(res http.ResponseWriter, req *http.Request) {
		err := collection.Update(
			bson.M{"_id": pat.Param(req, "id")},
			bson.M{"$set": bson.M{"lastKeepAlive": time.Now().UTC()}},
		)

		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})

	mux.HandleFunc(pat.Put("/api/jobs/:id/status"), func(res http.ResponseWriter, req *http.Request) {
		jobStatus := JobStatus{}
		err := json.NewDecoder(req.Body).Decode(&jobStatus)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		if !oneOf(jobStatus.Status, "running", "failed", "complete") {
			http.Error(res, "invalid job status, must be one of 'running', 'failed', 'complete'", 400)
			return
		}

		err = collection.Update(
			bson.M{"_id": pat.Param(req, "id")},
			bson.M{"$set": bson.M{"status": jobStatus.Status}},
		)

		if err != nil {

		}
	})

	mux.HandleFunc(pat.Get("/api/jobs/next"), func(res http.ResponseWriter, req *http.Request) {
		job := Job{}
		err := collection.Find(bson.M{"status": "queued"}).Sort("submittedAt").One(&job)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		err = json.NewEncoder(res).Encode(job)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})

	mux.HandleFunc(pat.Get("/api/jobs/:id"), func(res http.ResponseWriter, req *http.Request) {
		job := Job{}
		err := collection.Find(bson.M{"_id": pat.Param(req, "id")}).One(&job)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		err = json.NewEncoder(res).Encode(job)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})

	mux.HandleFunc(pat.Delete("/api/jobs/:id"), func(res http.ResponseWriter, req *http.Request) {
		err := collection.Remove(bson.M{"_id": pat.Param(req, "id")})
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})

	mux.HandleFunc(pat.Get("/api/jobs"), func(res http.ResponseWriter, req *http.Request) {
		jobs := []Job{}
		err := collection.Find(bson.M{}).Sort("-submittedAt").Limit(100).All(&jobs)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		err = json.NewEncoder(res).Encode(jobs)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	})
}
