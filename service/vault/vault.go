package vault

import (
	"errors"
	"fmt"

	vapi "github.com/hashicorp/vault/api"
)

// Config is the configuration required to create a new vault instance
type Config struct {
	Host     string
	LoginKey string

	Token string
	Role  string
	Env   string
}

// Vault is a logged-in vault instance
type Vault struct {
	client *vapi.Client
	Env    string
}

// ErrInvalidClientToken is used when the data returned by vault on login is invalid.
var ErrInvalidClientToken = errors.New("returned vault client token was invalid or blank")

// New creates a new vault instance by fetching the appropriate AppRole token
// and attaching it to the returned vault client instance.
func New(conf Config) (*Vault, error) {
	if conf.Env == "" {
		return nil, errors.New("failed to create vault client: field 'Env' cannot be empty")
	}

	client, err := vapi.NewClient(&vapi.Config{
		Address: conf.Host,
	})
	if err != nil {
		return nil, err
	}

	options := map[string]interface{}{
		"jwt":  conf.Token,
		"role": conf.Role,
	}

	login, err := client.Logical().Write(conf.LoginKey, options)
	if err != nil {
		return nil, err
	}

	if len(login.Auth.ClientToken) == 0 {
		return nil, ErrInvalidClientToken
	}

	client.SetToken(login.Auth.ClientToken)
	return &Vault{
		client: client,
		Env:    conf.Env,
	}, nil
}

// FetchValue fetches a value from vault by string key.
func (v *Vault) FetchValue(key string) (map[string]interface{}, error) {
	val, err := v.client.Logical().Read(fmt.Sprintf("secret/%s", key))
	if err != nil {
		return nil, err
	}

	return val.Data, nil
}
