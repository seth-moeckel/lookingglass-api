package database

import (
	"fmt"
	"io/ioutil"

	"os"
	"path/filepath"
	"strconv"
	"strings"

	"bitbucket.org/crosschx/lookingglass-api/service/log"
)

type migration struct {
	Version int64
	Running bool
}

const schemaSQL = `
CREATE TABLE IF NOT EXISTS db_version (
  id tinyint(1) unsigned NOT NULL DEFAULT 1,
  version int(10) unsigned NOT NULL DEFAULT 0,
  running tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin COMMENT='Used for automatic database versioning. Do not modify!';
`

// RunMigrations runs the database migrations provided by the MigrationDir from
// configuration, and updates the database (migration) version to the last run
// migration.
func (d *Database) RunMigrations(dir string, logger log.Logger) error {
	logger.Debug().Msg("starting migrations")
	m := migration{}
	err := d.Get(&m, "SELECT version, running FROM db_version WHERE id = 1")
	// ignore error "Error 1146: Table 'global_trigger_api.db_version' doesn't exist"
	// which is expected on the first run
	if err != nil && !strings.HasSuffix(err.Error(), ".db_version' doesn't exist") {
		return err
	}

	// if migrations are running, fail fast because the database is in an unexpected state
	if m.Running {
		return fmt.Errorf("migration '%d.sql' failed during a past run, stopping now", m.Version)
	}

	tx, err := d.Beginx()
	if err != nil {
		return err
	}
	defer tx.Commit()

	_, err = tx.Exec(schemaSQL)
	if err != nil {
		return err
	}

	// only inserts on the first run (ignores error)
	/* #nosec */
	tx.Exec("INSERT IGNORE INTO db_version (id, version, running) VALUES (1, 0, 0)")

	appliedCount := 0
	totalCount := 0
	skippedCount := 0

	err = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		// only run files with .sql extensions
		if filepath.Ext(path) != ".sql" {
			return nil
		}
		totalCount++
		filename := strings.TrimSuffix(filepath.Base(path), filepath.Ext(".sql"))
		version, err := strconv.ParseInt(filename, 10, 64)
		if err != nil {
			return err
		}

		if version <= m.Version {
			skippedCount++
			return nil
		}

		logger.Debug().Int64("version", version).Msg("applying migration")
		_, err = tx.Exec(fmt.Sprintf("UPDATE db_version SET running = 1, version = %d WHERE id = 1", version))
		if err != nil {
			return err
		}

		fileBytes, err := ioutil.ReadFile(filepath.Clean(path))
		if err != nil {
			return err
		}

		sqlBlocks := strings.Split(string(fileBytes), ";")
		for _, block := range sqlBlocks {
			block = strings.TrimSpace(block)
			if block != "" {
				_, err := tx.Exec(block)
				if err != nil {
					return err
				}
			}
		}

		appliedCount++

		logger.Debug().Int64("version", version).Msg("successfully applied migration")

		return nil
	})
	if err != nil {
		return err
	}

	_, err = tx.Exec("UPDATE db_version SET running = 0 WHERE id = 1")
	if err != nil {
		return err
	}

	logger.Debug().
		Int("totalCount", totalCount).
		Int("appliedCount", appliedCount).
		Int("skippedCount", skippedCount).
		Msg("finished migrations")

	return nil
}
