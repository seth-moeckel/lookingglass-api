package database

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
)

// type alias to avoid imports
type Tx = sqlx.Tx

func (d *Database) exec(readonly bool, cb func(*sqlx.Tx) error) error {
	opts := sql.TxOptions{ReadOnly: readonly, Isolation: sql.LevelSerializable}
	tx, err := d.BeginTxx(context.Background(), &opts)
	if err != nil {
		return err
	}

	err = cb(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	// only commit changes if the callback doesn't error out
	return tx.Commit()
}

// View provides a read-only transaction for executing reads
func (d *Database) View(cb func(*sqlx.Tx) error) error {
	return d.exec(true, cb)
}

// Update provides a transaction for executing mutating requests
// against the database. If the provided callback returns an error
// the transaction will not be committed.
func (d *Database) Update(cb func(*sqlx.Tx) error) error {
	return d.exec(false, cb)
}
