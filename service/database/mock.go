package database

import (
	"database/sql/driver"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
)

// MockTime specifies a mock sql argument for any time
type MockTime struct{}

// Match satisfies sqlmock.Argument interface
func (a MockTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

// MockArgs are argument placeholders
type MockArgs struct {
	Time MockTime
}

// MockDial returns a pointer to a mock database
func MockDial() (*Database, sqlmock.Sqlmock, MockArgs, error) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		return &Database{}, mock, MockArgs{}, err
	}
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	return &Database{sqlxDB}, mock, MockArgs{}, err
}
