package database

import (
	"fmt"

	// Mysql driver
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// Database is a simple wrapper around the database
// client connection
type Database struct {
	*sqlx.DB
}

// Config is the configuration required to create a database client connection.
type Config struct {
	DatabaseName string `required:"true" split_words:"true" json:"databaseName"`
	Host         string `required:"true" json:"host"`
	Port         string `default:"3306" json:"port"`
	Username     string `json:"-"`
	Password     string `json:"-"`
	MaxOpenConns int    `default:"50" split_words:"true" json:"maxOpenConns"`
	MaxIdleConns int    `default:"10" split_words:"true" json:"maxIdleConns"`
}

// Dial dials the database using the provided configuration object, returning
// a pointer to the client connection.
func Dial(conf Config) (*Database, error) {
	url := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True", conf.Username, conf.Password, conf.Host, conf.Port, conf.DatabaseName)

	// does not actually open a connection
	client, err := sqlx.Open("mysql", url)
	if err != nil {
		return nil, err
	}

	client.SetMaxOpenConns(conf.MaxOpenConns)
	client.SetMaxIdleConns(conf.MaxIdleConns)

	err = client.Ping()
	if err != nil {
		return nil, err
	}

	return &Database{client}, nil
}
