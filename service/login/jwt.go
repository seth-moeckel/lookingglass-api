package login

import (
	"errors"
	"fmt"

	"bitbucket.org/crosschx/lookingglass-api/service/vault"
)

// AuthHeaderKey is the string key used to get/set the header key storing
// request auth.
const AuthHeaderKey = "x-olive-auth"

var (
	// ErrInvalidJWTRecieved is used when the cookie returned from a login call to a peer
	// is either malformed or missing altogether.
	ErrInvalidJWTRecieved = errors.New("jwt returned from peer service is missing or invalid")
)

// ErrInvalidCredsReceived is used when the data returned by a fetch call to vault
// is not of the valid format, is missing fields, or has fields of the wrong datatype.
type ErrInvalidCredsReceived struct {
	FieldName string
}

func (e ErrInvalidCredsReceived) Error() string {
	return fmt.Sprintf(
		"credentials returned from vault are malformed or invalid: missing or invalid string field '%s'",
		e.FieldName,
	)
}

// JWT is a JSON Web Token for use in authenticated service to service requests.
type JWT string

// Credentials is an object containing the api keys required to
// retrieve a JWT from a peer service.
type Credentials struct {
	APIKey string
	Secret string
}

// FetchCreds retrieves api keys from vault.
func FetchCreds(v *vault.Vault) (Credentials, error) {
	// the key/path of service api keys used to make login
	// requests against peer services.
	creds, err := v.FetchValue(fmt.Sprintf("olive-manager-api/%s/serviceauth/lookingglass-api/creds", v.Env))
	if err != nil {
		return Credentials{}, err
	}

	apiKeyI, ok := creds["apiKey"]
	if !ok {
		return Credentials{}, ErrInvalidCredsReceived{"apiKey"}
	}

	apiKey, ok := apiKeyI.(string)
	if !ok {
		return Credentials{}, ErrInvalidCredsReceived{"apiKey"}
	}

	secretI, ok := creds["secret"]
	if !ok {
		return Credentials{}, ErrInvalidCredsReceived{"secret"}
	}

	secret, ok := secretI.(string)
	if !ok {
		return Credentials{}, ErrInvalidCredsReceived{"secret"}
	}

	return Credentials{
		APIKey: apiKey,
		Secret: secret,
	}, nil
}
