package job

import (
	"context"
	"errors"
	"strings"

	"github.com/google/uuid"

	"time"

	"bitbucket.org/crosschx/lookingglass-api/service/database"
)

// TODO add explicit fields to SQL

const (
	StatusQueued   = "queued"
	StatusRunning  = "running"
	StatusComplete = "complete"
	StatusFailed   = "failed"
)

type JobInput struct {
	PerspectiveName string   `json:"perspectiveName"`
	Rulescript      string   `json:"rulescript"`
	RuleGlobs       []string `json:"ruleGlobs"`
}

type JobStatus struct {
	Status string `json:"status"`
}

type Job struct {
	ID              string    `json:"id" db:"id"`
	PerspectiveName string    `json:"perspectiveName" db:"perspective_name"`
	Rulescript      string    `json:"rulescript" db:"rulescript"`
	RuleGlobs       []string  `json:"ruleGlobs" db:"-"`
	RawGlobsString  string    `json:"-" db:"globs"`
	Status          string    `json:"status" db:"status"`
	SubmittedAt     time.Time `json:"submittedAt" db:"submitted_at"`
	LastKeepAlive   time.Time `json:"lastKeepAlive" db:"last_keep_alive"`
}

type jobGlob struct {
	ID    string `db:"id"`
	JobID string `db:"job_id"`
	Glob  string `db:"glob"`
}

func oneOf(key string, values ...string) bool {
	for _, val := range values {
		if key == val {
			return true
		}
	}

	return false
}

func Create(ctx context.Context, db *database.Database, jobInput JobInput) (Job, error) {
	job := Job{
		ID:              uuid.New().String(),
		PerspectiveName: jobInput.PerspectiveName,
		Rulescript:      jobInput.Rulescript,
		RuleGlobs:       jobInput.RuleGlobs,
		Status:          StatusQueued,
		SubmittedAt:     time.Now().UTC(),
		LastKeepAlive:   time.Now().UTC(),
	}

	query := `
	INSERT INTO job (
		id,
		perspective_name,
		rulescript,
		status,
		submitted_at,
		last_keep_alive
	) VALUES (
		:id,
		:perspective_name,
		:rulescript,
		:status,
		:submitted_at,
		:last_keep_alive
	)
	`

	globsQuery := `
	INSERT INTO job_glob (
		id,
		job_id,
		glob
	) VALUES (
		:id,
		:job_id,
		:glob
	)
	`

	err := db.Update(func(tx *database.Tx) error {
		_, err := tx.NamedExecContext(ctx, query, job)
		if err != nil {
			return err
		}

		for _, glob := range job.RuleGlobs {
			_, err := tx.NamedExecContext(ctx, globsQuery, jobGlob{
				ID:    uuid.New().String(),
				JobID: job.ID,
				Glob:  glob,
			})

			if err != nil {
				return err
			}
		}

		return err
	})

	return job, err
}

func GetAll(db *database.Database) ([]Job, error) {
	query := `
	SELECT
		j.id,
		j.perspective_name,
		j.rulescript,
		j.status,
		j.submitted_at,
		j.last_keep_alive,
		GROUP_CONCAT(g.glob) as globs
	FROM job j
		LEFT JOIN job_glob g ON j.id = g.job_id
	GROUP BY j.id
	ORDER BY j.submitted_at DESC
	LIMIT 100
	`

	jobs := []Job{}
	return jobs, db.View(func(tx *database.Tx) error {
		rows, err := db.Queryx(query)
		if err != nil {
			return err
		}

		for rows.Next() {
			var j Job
			err = rows.StructScan(&j)
			if err != nil {
				return err
			}

			j.RuleGlobs = strings.Split(j.RawGlobsString, ",")
			jobs = append(jobs, j)
		}

		return nil
	})
}

func GetNext(ctx context.Context, db *database.Database) (Job, error) {
	query := `
	SELECT
		j.id,
		j.perspective_name,
		j.rulescript,
		j.status,
		j.submitted_at,
		j.last_keep_alive,
		GROUP_CONCAT(g.glob) as globs
	FROM job j
		LEFT JOIN job_glob g ON j.id = g.job_id
	WHERE j.status = "queued"
	GROUP BY j.id
	ORDER BY j.submitted_at DESC
	LIMIT 1
	`

	jobs := []Job{}
	err := db.View(func(tx *database.Tx) error {
		rows, err := db.QueryxContext(ctx, query)
		if err != nil {
			return err
		}

		for rows.Next() {
			var j Job
			err = rows.StructScan(&j)
			if err != nil {
				return err
			}

			j.RuleGlobs = strings.Split(j.RawGlobsString, ",")
			jobs = append(jobs, j)
		}

		return nil
	})

	if err != nil {
		return Job{}, err
	}

	if len(jobs) == 0 {
		return Job{}, errors.New("record not found")
	}

	return jobs[0], nil
}

func GetByID(ctx context.Context, db *database.Database, id string) (Job, error) {
	query := `
	SELECT
		j.id,
		j.perspective_name,
		j.rulescript,
		j.status,
		j.submitted_at,
		j.last_keep_alive,
		GROUP_CONCAT(g.glob) as globs
	FROM job j
		LEFT JOIN job_glob g ON j.id = g.job_id
	WHERE j.id = ?
	GROUP BY j.id
	ORDER BY j.submitted_at DESC
	LIMIT 1
	`

	jobs := []Job{}
	err := db.View(func(tx *database.Tx) error {
		rows, err := db.QueryxContext(ctx, query, id)
		if err != nil {
			return err
		}

		for rows.Next() {
			var j Job
			err = rows.StructScan(&j)
			if err != nil {
				return err
			}

			j.RuleGlobs = strings.Split(j.RawGlobsString, ",")
			jobs = append(jobs, j)
		}

		return nil
	})

	if err != nil {
		return Job{}, err
	}

	if len(jobs) == 0 {
		return Job{}, errors.New("record not found")
	}

	return jobs[0], nil
}

func DeleteByID(ctx context.Context, db *database.Database, id string) error {
	return db.Update(func(tx *database.Tx) error {
		_, err := tx.QueryxContext(ctx, "DELETE FROM `job` WHERE id = ?", id)
		return err
	})
}

func UpdateStatusByID(ctx context.Context, db *database.Database, id string, status string) error {
	if !oneOf(status, StatusRunning, StatusComplete, StatusFailed) {
		return errors.New("status must be oneOf 'running', 'complete', 'failed'")
	}

	return db.Update(func(tx *database.Tx) error {
		_, err := tx.QueryxContext(ctx, "UPDATE job set status = ? WHERE id = ?", status, id)
		return err
	})
}

func UpdateKeepAliveByID(ctx context.Context, db *database.Database, id string) error {
	return db.Update(func(tx *database.Tx) error {
		_, err := tx.QueryxContext(ctx, "UPDATE job set last_keep_alive = ? WHERE id = ?", time.Now().UTC(), id)
		return err
	})
}
