package log

import (
	"io"
	"os"
	"time"

	"github.com/rs/zerolog"
)

//Logger is a wrapper of zerolog.Logger
type Logger struct {
	zerolog.Logger
}

//Level is the log level.
type Level string

//ZerologLevel returns the mapped level to the zero log level.
func (l Level) ZerologLevel() zerolog.Level {
	switch l {
	case LevelInfo:
		return zerolog.InfoLevel
	case LevelDebug:
		return zerolog.DebugLevel
	case LevelWarn:
		return zerolog.WarnLevel
	case LevelError:
		return zerolog.ErrorLevel
	}

	// default to the lowest level
	return zerolog.DebugLevel
}

const (
	//LevelInfo is used for all info logs.
	LevelInfo Level = "info"
	//LevelDebug is used for all debug logs.
	LevelDebug Level = "debug"
	//LevelWarn is used for all warn logs.
	LevelWarn Level = "warn"
	//LevelError is used for all error logs.
	LevelError Level = "error"
)

//New returns back a new instance of logger.
func New(pretty bool, module string, level Level) Logger {
	var output io.Writer = os.Stdout
	if pretty {
		output = zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	}

	return Logger{
		zerolog.New(output).With().
			Str("mod", module).
			Timestamp().
			Logger().Level(zerolog.Level(level.ZerologLevel()))}
}
