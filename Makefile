GIT_COMMIT := $(shell git rev-list -1 HEAD)
GIT_BRANCH := $(if $(shell echo $$BRANCH_NAME),$(shell echo $$BRANCH_NAME),$(shell git rev-parse --abbrev-ref HEAD))
VERSION := $(if $(shell echo $$VERSION),$(shell echo $$VERSION),"localdev-unset")

list:
	@echo "build - builds the application binary"
	@echo "run - runs the application locally"
	@echo "test - runs the application unit tests"
	@echo "docker - builds the application binary and creates a docker image for it"
	@echo "lint - runs the golang linter"
	@echo "static-check - runs static code analysis"

build:
	-rm -rf bin
	mkdir -p bin
	GO111MODULE=off GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
		go build \
			-ldflags "-X main.GitCommit=${GIT_COMMIT} -X main.GitBranch=${GIT_BRANCH} -X main.Version=${VERSION}" \
			-o bin/api main.go

run:
	GO111MODULE=off go build \
		-ldflags "-X main.GitCommit=${GIT_COMMIT} -X main.GitBranch=${GIT_BRANCH} -X main.Version=${VERSION}" \
		-o bin/api-dev main.go

	LGA_LOCAL=true \
	POD_NAMESPACE=dev \
	LGA_LOG_LEVEL=debug \
	LGA_DB_DATABASE_NAME=looking_glass_api \
	LGA_DB_HOST=localhost \
	LGA_DB_USERNAME=crosschx \
	LGA_DB_PASSWORD=crosschx \
	LGA_HTTP_LISTEN_ADDR=localhost:5000 \
	LGA_DB_PORT=3306 \
	./bin/api-dev

test:
	CGO_ENABLED=0 go test -v ./... -cover

test-profile:
	GO111MODULE=off CGO_ENABLED=0 go test ./... -coverprofile testCoverageAnalysis.out
	go tool cover -html=testCoverageAnalysis.out
	rm testCoverageAnalysis.out

docker: build
	docker build -t olive/lookingglass-api:latest .

lint:
	go get golang.org/x/lint/golint
	go list ./... | grep -v /vendor/ | xargs -L1 $$GOPATH//bin/golint -set_exit_status

static-check:
	go get -u honnef.co/go/tools/cmd/staticcheck
	CGO_ENABLED=0 $$GOPATH/bin/staticcheck $(go list ./... | grep -v /vendor/)

gosec:
	go get github.com/securego/gosec/cmd/gosec/...
	CGO_ENABLED=0 $$GOPATH/bin/gosec -quiet ./config/... ./controller/... ./middlewares/... ./routes/... ./service/...

.PHONY: build dev docker int-test lint staticCheck gosec
